package main

import (
	"context"
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"time"

	"github.com/go-redis/redis"
)

var client = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "",
	DB:       0,
})

var (
	// Posts are user posts
	Posts     = make(map[string][]*Post)
	keyMap    = make(map[string]*formKeys)
	templates = template.Must(template.New("main").ParseGlob("internal/tmpl/*.tmpl")) // [0]
	reg       = regexp.MustCompile("^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$")
)

type formKeys struct {
	Key string
	TS  time.Time
}

// User consists of just the IP for identification. Later it will probably be
// better to add a complete fingerprint.
type User struct {
	IP string `json:"IP"`
}

// Post is a new thread or reply to a thread
type Post struct {
	Body     string  `json:"body"`
	Title    string  `json:"title"`
	ID       string  `json:"ID"`
	Children []*Post `json:"children"`
	Parent   string  `json:"parent"`
	Random   string
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	// Any package you import, directly or through other dependencies, has access
	// to http.DefaultServeMux and might register routes you don’t expect,
	// because of this we define our own serve mux.
	// (https://blog.gopheracademy.com/advent-2016/exposing-go-on-the-internet/)
	mux := http.NewServeMux()
	// mux.Handle("/", http.HandlerFunc(home))
	mux.HandleFunc("/", home)
	mux.HandleFunc("/view/", view)
	mux.HandleFunc("/api/reply/", newReply)
	mux.HandleFunc("/api/thread/", newThread)
	mux.Handle("/public/", http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))

	// Server configuration
	srv := &http.Server{
		Addr:              ":7654",
		Handler:           mux,
		ReadHeaderTimeout: 5 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       5 * time.Second,
	}

	// Start accepting connections
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			fmt.Println(err)
		}
		// if err := srv.ListenAndServeTLS("./auth/cert.pem", "./auth/key.pem"); err != nil {
		// 	fmt.Println(err)
		// }
	}()

	fmt.Println("Server started @ " + srv.Addr)

	// Create a channel to listen for an interrupt and trigger a graceful shutdown
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	// The server will immediately exit unless we use this channel as a blocking
	// mechanism to wait for the interrupt signal (ctrl+c)
	// NOTE: You won't see logging if you run this from within vim
	<-stopChan
	fmt.Println("Received interrupt signal, halting server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err := srv.Shutdown(ctx)
	if err != nil {
		fmt.Println("ERROR shutting down server: ", err)
		return
	}
	fmt.Println("Server halted gracefully")
}

func newThread(w http.ResponseWriter, r *http.Request) {
	parent := strings.Split(r.URL.RequestURI(), ":")
	rand := r.PostFormValue("hidden")
	if val, ok := keyMap[r.RemoteAddr]; ok && val.Key == rand {
		delete(keyMap, r.RemoteAddr)
		body := r.PostFormValue("postBody")
		title := r.PostFormValue("title")
		if reg.MatchString(title) {
			pid := genPostID()
			post := map[string]interface{}{
				"data":   body,
				"title":  title,
				"ID":     pid,
				"parent": "BOARD:" + parent[len(parent)-1:][0],
			}
			zmem := redis.Z{
				Member: pid,
				Score:  0,
			}
			client.HMSet("obj:"+pid, post)
			client.ZAdd("BOARD:"+parent[len(parent)-1:][0], zmem)

			getData(r)
			http.Redirect(w, r, "/", 301)
			return
		}
	} else {
		http.Error(w, "guru is dead", http.StatusInternalServerError)
	}

}

func genPostID() (ID string) {
	symbols := "abcdefghijklmnopqrstuvwxyz1234567890"
	for i := 0; i <= 10; i++ {
		randum := genRand(0, len(symbols))
		ID += symbols[randum : randum+1]
	}
	return
}

func genRand(min, max int) int {
	if max <= 0 || min == max || min < 0 {
		return 0
	}
	return rand.Intn(max-min) + min
}

func newReply(w http.ResponseWriter, r *http.Request) {
	rand := r.PostFormValue("hidden")
	parent := strings.Split(r.URL.RequestURI(), ":")
	p1 := parent[len(parent)-1:][0]
	fmt.Println("parent", p1)
	ip := strings.Split(r.RemoteAddr, ":")[0]
	fmt.Println(ip, keyMap[ip].Key, rand)
	// if val, ok := keyMap[strings.Split(r.RemoteAddr, ":")[0]]; ok && val.Key == rand {
	if val, ok := keyMap[r.RemoteAddr]; ok && val.Key == rand {
		delete(keyMap, r.RemoteAddr)
		body := r.PostFormValue("postBody")
		title := r.PostFormValue("title")
		if reg.MatchString(title) {
			fmt.Println(p1)
			pid := genPostID()
			post := map[string]interface{}{
				"data":   body,
				"title":  title,
				"ID":     pid,
				"parent": parent[len(parent)-1:][0],
			}
			zmem := redis.Z{
				Member: pid,
				Score:  0,
			}
			client.HMSet("obj:"+pid, post)
			fmt.Println("'"+parent[len(parent)-1:][0]+"'", zmem)
			client.ZAdd(parent[len(parent)-1:][0], zmem)
			pa := client.HMGet("obj:"+parent[len(parent)-1:][0], "parent").Val()
			fmt.Println(pa)
			client.ZIncrBy(pa[0].(string), 1, parent[len(parent)-1:][0])

			getData(r)
			http.Redirect(w, r, "/view/?postNum="+pid, 301)
			return
		}
	} else {
		http.Error(w, "guru is dead", http.StatusInternalServerError)
	}
}

func randKey(length int, ip string) string {
	p := "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	r := make([]byte, length)
	for i := range r {
		r[i] = p[rand.Intn(len(p))]
	}
	keyMap[ip] = &formKeys{Key: string(r), TS: time.Now()}
	return string(r)
}

type boardData struct {
	ID     string
	Random string
	Posts  []*Post
}

func home(w http.ResponseWriter, r *http.Request) {
	// fmt.Println(r.URL.RequestURI())
	// rk := randKey(10, strings.Split(r.RemoteAddr, ":")[0])
	rk := randKey(10, r.RemoteAddr)
	// fmt.Println(keyMap[strings.Split(r.RemoteAddr, ":")[0]].Key)
	getData(r)
	board := strings.ToUpper(r.URL.RequestURI()[1:])
	if board == "POLITICS" || board == "STEM" || board == "ARTS" || board == "BUSINESS" || board == "OTHER" {
		d := &boardData{
			ID:     "BOARD:" + board,
			Random: rk,
			Posts:  Posts["BOARD:"+board],
		}
		// fmt.Println(d.Random)
		err := templates.ExecuteTemplate(w, "board.tmpl", d)
		if err != nil {
			fmt.Println(err)
		}

		return
	}

	d := &boardData{
		ID:     "BOARD:POLITICS",
		Random: rk,
		Posts:  Posts["BOARD:POLITICS"],
	}
	// fmt.Println(d.Random)
	err := templates.ExecuteTemplate(w, "board.tmpl", d)
	if err != nil {
		fmt.Println(err)
	}

}

type threadData struct {
	Random   string
	Thread   *Post
	Children []*Post
}

func view(w http.ResponseWriter, r *http.Request) {
	var p *Post
	r.ParseForm()
	data := client.HGetAll("obj:" + r.Form["postNum"][0]).Val()
	childs := getChildren(r.Form["postNum"][0], r)
	if data["data"] != "" && data["ID"] != "" && data["title"] != "" {
		p = &Post{
			Body:   data["data"],
			Title:  data["title"],
			ID:     data["ID"],
			Random: keyMap[r.RemoteAddr].Key,
		}
	}
	// fmt.Println(p)
	d := &threadData{
		// Random:   keyMap[strings.Split(r.RemoteAddr, ":")[0]].Key,
		Random:   keyMap[r.RemoteAddr].Key,
		Thread:   p,
		Children: childs,
	}
	err := templates.ExecuteTemplate(w, "thread.tmpl", d)
	if err != nil {
		fmt.Println(err)
	}
}

func getChildren(ID string, r *http.Request) (childs []*Post) {
	children := client.ZRevRangeByScore(ID, redis.ZRangeBy{Max: "100000"}).Val()
	for _, v := range children {
		data, _ := client.HGetAll("obj:" + v).Result()
		childs = append(childs, &Post{
			Body:     data["data"],
			ID:       data["ID"],
			Title:    data["title"],
			Parent:   ID,
			Children: getChildren(data["ID"], r),
			Random:   keyMap[r.RemoteAddr].Key,
			// Random:   keyMap[strings.Split(r.RemoteAddr, ":")[0]].Key,
		})
	}
	return
}

func getData(r *http.Request) {
	Posts = make(map[string][]*Post)
	boards := client.ZRange("BOARD", 0, -1)
	// board1 := strings.ToUpper(r.URL.RequestURI()[1:])
	// boards := client.ZRangeByScore("BOARD", redis.ZRangeBy{})
	for _, board := range boards.Val() {
		posts := client.ZRevRangeByScore(board, redis.ZRangeBy{Max: "100000"})
		// posts := client.ZRange(board, 0, -1)
		fmt.Println("posts::::", posts)
		for _, post := range posts.Val() {
			data, _ := client.HGetAll("obj:" + post).Result()
			th := &Post{
				ID:       data["ID"],
				Body:     data["data"],
				Title:    data["title"],
				Children: getChildren(post, r),
				Parent:   data["parent"],
				Random:   keyMap[r.RemoteAddr].Key,
				// Random:   keyMap[strings.Split(r.RemoteAddr, ":")[0]].Key,
			}
			Posts[board] = append(Posts[board], th)
		}
	}
}
